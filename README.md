# Visual Review Tools Screenshot Proof of Concept

### Running project

This is a static site, and the way that I've been running this is through python `SimpleHTTPServer`

Run `python -m SimpleHTTPServer 8000` in the root of this directory. Then go to `http://127.0.0.1:8000/` in your browser.
